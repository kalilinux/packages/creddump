Source: creddump
Section: utils
Priority: extra
Maintainer: Kali Developers <devel@kali.org>
Build-Depends: debhelper (>= 8.0.0)
Standards-Version: 3.9.3
Homepage: http://code.google.com/p/creddump/
Vcs-Git: https://gitlab.com/kalilinux/packages/creddump.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/creddump

Package: creddump
Architecture: all
Depends: ${misc:Depends}, python, python-crypto
Description: Extracts credentials from Windows registry hives
 creddump is a python tool to extract various credentials and secrets from
 Windows registry hives. It currently extracts:
 * LM and NT hashes (SYSKEY protected)
 * Cached domain passwords
 * LSA secrets
 .
 It essentially performs all the functions that bkhive/samdump2,
 cachedump, and lsadump2 do, but in a platform-independent way.
 .
 It is also the first tool that does all of these things in an offline
 way (actually, Cain & Abel does, but is not open source and is only
 available on Windows).
